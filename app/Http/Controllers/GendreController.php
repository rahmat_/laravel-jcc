<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GendreController extends Controller
{
    public function create(){
        return view('gendre.create');
    }

    public function store(Request $request){
        $request->validate(
            [
                'nama' => 'required'
            ],
            [
                'nama.required' => 'Inputan Nama Harus Diisi'
            ]
        );

        DB::table('gendre')->insert(
            [
                'nama' => $request['nama']
            ]
        );

        return redirect('/gendre');
    }

    public function index(){
        $gendre = DB::table('gendre')->get();
 
        return view('gendre.index', ['gendre' => $gendre]);
    }

    public function show($id){
        $gendre = DB::table('gendre')->where('id', $id)->first();

        return view('gendre.show', compact('gendre'));
    }

    public function edit($id){
        $gendre = DB::table('gendre')->where('id', $id)->first();

        return view('gendre.edit', compact('gendre'));
    }

    public function update($id, Request $request){
        $request->validate(
            [
                'nama' => 'required'
            ],
            [
                'nama.required' => 'Inputan Nama Harus Diisi'
            ]
        );

        DB::table('gendre')->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama']
                ]
            );
        
        return redirect('/gendre');
    }

    public function destroy($id){
        DB::table('gendre')->where('id', '=', $id)->delete();
        return redirect('/gendre');
    }

}
