<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    public function index(){
        $game = DB::table('game')->get();
 
        return view('game.index', ['game' => $game]);
    }

    public function create(){
        return view('game.create');
    }

    public function store(Request $request){
        $request->validate(
            [
                'name' => 'required',
                'gameplay' => 'required',
                'developer' => 'required',
                'year' => 'required',
            ],
            [
                'name.required' => 'Inputan Nama Game Harus Diisi',
                'gameplay.required'  => 'Inputan Gameplay Harus Diisi',
                'developer.required'  => 'Inputan Developer Harus Diisi',
                'year.required'  => 'year Harus Diisi',
            ]
        );

        DB::table('game')->insert(
            [
                'name' => $request['name'],
                'gameplay' => $request['gameplay'],
                'developer' => $request['developer'],
                'year' => $request['year']
            ]
        );

        return redirect('/game');
    }

    public function show($id){
        $game = DB::table('game')->where('id', $id)->first();

        return view('game.show', compact('game'));
    }

    public function edit($id){
        $game = DB::table('game')->where('id', $id)->first();

        return view('game.edit', compact('game'));
    }

    public function update($id, Request $request){
        $request->validate(
            [
                'name' => 'required',
                'gameplay' => 'required',
                'developer' => 'required',
                'year' => 'required',
            ],
            [
                'name.required' => 'Inputan Nama Game Harus Diisi',
                'gameplay.required'  => 'Inputan Gameplay Harus Diisi',
                'developer.required'  => 'Inputan Developer Harus Diisi',
                'year.required'  => 'year Harus Diisi',
            ]
        );

        DB::table('game')->where('id', $id)
            ->update(
                [
                    'name' => $request['name'],
                    'gameplay' => $request['gameplay'],
                    'developer' => $request['developer'],
                    'year' => $request['year']
                ]
            );
        
        return redirect('/game');
    }

    public function destroy($id){
        DB::table('game')->where('id', '=', $id)->delete();
        return redirect('/game');
    }
}
