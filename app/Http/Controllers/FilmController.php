<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gendre;
use App\Film;
use File;

class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();
        return view('film.index', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gendre = Gendre::all();

        return view('film.create', compact('gendre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'judul' => 'required',
                'ringkasan' => 'required',
                'tahun' => 'required',
                'gendre_id' => 'required',
                'poster' => 'required|mimes:jpeg,jpg,,png|max:2200',
            ],
            [
                'judul.required' => 'Inputan Judul Harus Diisi',
                'ringkasan.required' => 'Inputan Ringkasan Harus Diisi',
                'tahun.required' => 'Inputan Tahun Harus Diisi',
                'gendre_id.required'  => 'Inputan Gendre Harus Diisi',
                'poster.required'  => 'Poster Harus Diisi',
                'poster.mimes'  => 'Poster Hanya bisa format jpeg, jpg, png',
            ]
        );

        $gambar = $request->poster;
        $new_gambar = time() . '-' . $gambar->getClientOriginalName();

        $film = new Film;
        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->gendre_id = $request->gendre_id;
        $film->poster = $new_gambar;
        $film->save();

        $gambar->move('poster/', $new_gambar);

        return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::findOrFail($id);

        return view('film.show', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::findOrFail($id);
        $gendre = Gendre::all();

        return view('film.edit', compact('film', 'gendre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'judul' => 'required',
                'ringkasan' => 'required',
                'tahun' => 'required',
                'gendre_id' => 'required',
                'poster' => 'mimes:jpeg,jpg,,png|max:2200',
            ],
            [
                'judul.required' => 'Inputan Judul Harus Diisi',
                'ringkasan.required' => 'Inputan Ringkasan Harus Diisi',
                'tahun.required' => 'Inputan Tahun Harus Diisi',
                'gendre_id.required'  => 'Inputan Gendre Harus Diisi',
                'poster.mimes'  => 'Poster Hanya bisa format jpeg, jpg, png',
            ]
        );

        $film = Film::find($id);

        if ($request->has('poster')) {
            $path = "poster/";
            File::delete($path . $film->poster);
            $gambar = $request->poster;
            $new_gambar = time() . '-' . $gambar->getClientOriginalName();
            $gambar->move('poster/', $new_gambar);

            $film->poster = $new_gambar;
        }

        $film->judul = $request->judul;
        $film->ringkasan = $request->ringkasan;
        $film->tahun = $request->tahun;
        $film->gendre_id = $request->gendre_id;

        $film->save();
        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id);
 

        $path = "poster/";
        File::delete($path . $film->poster);
        $film->delete();
        return redirect('/film');
    }
}
