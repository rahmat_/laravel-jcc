@extends('layout.master')
@section('judul')
    Halaman Register
@endsection
@section('content')
    <h4>Buat Account Baru</h4>
    <h5>Sign Up Form</h5>

    <form action="/welcome" method="post">
        @csrf
        <label>First name :</label><br><br>
        <input type="text" name="first_name"><br><br>
        <label >Last name :</label><br><br>
        <input type="text" name="last_name"><br><br>

        <label>Gender</label><br><br>
        <input type="radio" name="gender" value="Male">Male<br>
        <input type="radio" name="gender" value="Female">Female<br><br>

        <label>Nationality</label><br><br>
        <select name="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="inggris">Inggris</option>
        </select><br><br>

        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="language" value="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="language" value="English">English<br>
        <input type="checkbox" name="language" value="Other">Other<br><br>

        <label>Bio</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br><br>

        <input type="submit" value="Sign Up"><br>
    </form>
@endsection