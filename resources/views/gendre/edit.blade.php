@extends('layout.master')
@section('judul')
    Halaman Edit Gendre
@endsection

@section('content')
    <form action="/gendre/{{$gendre->id}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama Gendre</label>
            <input type="text" value="{{$gendre->nama}}"  class="form-control" name="nama">
        </div>

        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection