@extends('layout.master')
@section('judul')
    Halaman Detail Gendre
@endsection

@section('content')
    <div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">{{$gendre->nama}}</h2><br><br>
                <a href="/gendre" class="btn btn-primary">Kembali</a>
            </div>
        </div>
    </div>
@endsection