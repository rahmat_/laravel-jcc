@extends('layout.master')
@section('judul')
    Halaman Edit Cast/Pemeran
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama Pemeran</label>
            <input type="text" value="{{$cast->nama}}"  class="form-control" name="nama">
        </div>

        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Umur Pemeran</label>
            <input type="number" value="{{$cast->umur}}" class="form-control" name="umur">
        </div>

        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Biodata Pemeran</label>
            <textarea class="form-control" name="bio">{{$cast->bio}}</textarea>
        </div>

        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection