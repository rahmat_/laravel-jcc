@extends('layout.master')
@section('judul')
    Halaman Detail Cast/Pemeran
@endsection

@section('content')
    <div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">{{$cast->nama}}</h2>
                <p class="card-text">{{$cast->umur}}<br>{{$cast->bio}}</p>
                <a href="/cast" class="btn btn-primary">Kembali</a>
            </div>
        </div>
    </div>
@endsection