@extends('layout.master')
@section('judul')
    Halaman List Film
@endsection

@section('content')

    <a href="/film/create" class="btn btn-success my-3"> Tambah Film </a>

    <div class="row">
        @forelse ($film as $item)
            <div class="col-4">
                <div class="card">
                    <img src="{{asset('poster/'.$item->poster)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h3>{{$item->judul}}</h3>
                        <h5>({{$item->tahun}})</h5>
                        <p class="card-text">{{ Str::limit($item->ringkasan, 25) }}</p>
                        <form action="/film/{{$item->id}}" method="POST">
                            @method('delete')
                            @csrf
                            <a href="film/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="film/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                        </form>
                    </div>
                </div>
            </div>
        @empty
            <h4 style="color: red;">Data Kosong</h4>
        @endforelse
    </div>
@endsection
            