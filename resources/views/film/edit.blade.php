@extends('layout.master')
@section('judul')
    Halaman Edit Film
@endsection

@section('content')
    <form action="/film/{{$film->id}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Judul Film</label>
            <input type="text" value="{{$film->judul}}" class="form-control" name="judul">
        </div>

        @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Ringkasan Film</label>
            <textarea class="form-control" name="ringkasan">{{$film->ringkasan}}</textarea>
        </div>

        @error('ringkasan')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Tahun</label>
            <input type="number" value="{{$film->tahun}}" class="form-control" name="tahun">
        </div>

        @error('tahun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Gendre Film</label>
            <select name="gendre_id" id="" class="form-control">
                <option value="">--Pilih Gendre--</option>
                @foreach ($gendre as $item)
                    @if ($item->id === $film->gendre_id)
                        <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                    @else
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                    @endif
                    
                @endforeach
            </select>
        </div>

        @error('gendre_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Poster Film</label>
            <input type="file" class="form-control" name="poster">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection