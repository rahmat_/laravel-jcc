@extends('layout.master')
@section('judul')
    Halaman Detail Film
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <img src="{{asset('poster/'.$film->poster)}}" width="500" height="400" alt="...">
            <h3>{{$film->judul}}</h3>
            <h5>({{$film->tahun}})</h5>
            <p class="card-text">{{$film->ringkasan}}</p>
            <a href="/film" class="btn btn-info btn-sm">Kembali</a>
        </div>
    </div>  
@endsection