<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/table', function(){
    return view('halaman.table');
});

Route::get('/data-tables', function(){
    return view('halaman.data-table');
});

Route::get('/', 'HomeController@dashboard');
Route::get('/register', 'AuthController@pendataan');
Route::post('/welcome', 'AuthController@welcome');

//CRUD Cast
//mengarah create data
Route::get('/cast/create', 'CastController@create');
//menyimpan data ke database cast
Route::post('/cast', 'CastController@store');
//menampilkan seluruh data table cast
Route::get('/cast', 'CastController@index');
//menampilkan detail data
Route::get('/cast/{cast_id}', 'CastController@show');
//menampilkan form edit data
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
//menyimpan perubahan data ke table cast
Route::put('/cast/{cast_id}', 'CastController@update');
//menghapus data dengan id tertentu
Route::delete('/cast/{cast_id}', 'CastController@destroy');

//CRUD Gendre
//mengarah create data
Route::get('/gendre/create', 'GendreController@create');
//menyimpan data ke database gendre
Route::post('/gendre', 'GendreController@store');
//menampilkan seluruh data table gendre
Route::get('/gendre', 'GendreController@index');
//menampilkan detail data
Route::get('/gendre/{gendre_id}', 'GendreController@show');
//menampilkan form edit data
Route::get('/gendre/{gendre_id}/edit', 'GendreController@edit');
//menyimpan perubahan data ke table gendre
Route::put('/gendre/{gendre_id}', 'GendreController@update');
//menghapus data dengan id tertentu
Route::delete('/gendre/{gendre_id}', 'GendreController@destroy');

// //CRUD TOKOGAME
// //mengarah create data
// Route::get('/game/create', 'GameController@create');
// //menyimpan data ke database 
// Route::post('/game', 'GameController@store');
// //menampilkan seluruh data table 
// Route::get('/game', 'GameController@index');
// //menampilkan detail data
// Route::get('/game/{game_id}', 'GameController@show');
// //menampilkan form edit data
// Route::get('/game/{game_id}/edit', 'GameController@edit');
// //menyimpan perubahan data ke table 
// Route::put('/game/{game_id}', 'GameController@update');
// //menghapus data dengan id tertentu
// Route::delete('/game/{game_id}', 'GameController@destroy');

Route::resource('film', 'FilmController');


